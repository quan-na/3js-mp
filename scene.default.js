scene = typeof scene == 'undefined' ? {} : scene;
scene.default = (function() {
	var xArr = [0.0, 1.4, 1.5, 1.6, 3.0];
	var yArr = [0.0, 0.2, 0.4];
	var zArr = [0.0, 1.4, 1.5, 1.6, 3.0];
	if (typeof THREE != 'undefined') {
		var defaultMat = new THREE.MeshPhongMaterial( { color: 0x001484,
													 shininess: 0.1,
													 specular: 0x010101, } );
	}
	return function(drawType, cs) {
		if (['x','y','z'].indexOf(drawType) != -1) {
			var xs, ys;
			[xs, ys] = bpAxesConvert(drawType, xArr, yArr, zArr);
			if (drawType == 'z') {
				cs.beginPath();
				cs.moveTo(xs[0], ys[0]); cs.lineTo(xs[4], ys[0]);
				cs.moveTo(xs[2], ys[0]);
				{
					cs.lineTo(xs[3], ys[1]);
					cs.lineTo(xs[2], ys[2]);
					cs.lineTo(xs[1], ys[1]);
					cs.lineTo(xs[2], ys[0]);
				}
				cs.moveTo(xs[1], ys[1]); cs.lineTo(xs[3], ys[1]);
				cs.moveTo(xs[2], ys[0]); cs.lineTo(xs[2], ys[2]);
				cs.stroke();
			} else if (drawType == 'x') {
				cs.beginPath();
				cs.moveTo(xs[0], ys[0]); cs.lineTo(xs[4], ys[0]);
				cs.moveTo(xs[2], ys[0]);
				{
					cs.lineTo(xs[3], ys[1]);
					cs.lineTo(xs[2], ys[2]);
					cs.lineTo(xs[1], ys[1]);
					cs.lineTo(xs[2], ys[0]);
				}
				cs.moveTo(xs[1], ys[1]); cs.lineTo(xs[3], ys[1]);
				cs.moveTo(xs[2], ys[0]); cs.lineTo(xs[2], ys[2]);
				cs.stroke();
			} else { // y
				cs.beginPath();
				cs.strokeRect(xs[0], ys[0], xs[4], ys[4]);
				cs.moveTo(xs[1], ys[2]);
				{
					cs.lineTo(xs[2], ys[1]);
					cs.lineTo(xs[3], ys[2]);
					cs.lineTo(xs[2], ys[3]);
					cs.lineTo(xs[1], ys[2]);
				}
				cs.moveTo(xs[1], ys[2]); cs.lineTo(xs[3], ys[2]);
				cs.moveTo(xs[2], ys[1]); cs.lineTo(xs[2], ys[3]);
				cs.stroke();
			}
			return cs;
		} else {
			var geo = geoMesh().stripe([
				xArr[0], yArr[0], zArr[0],
				xArr[4], yArr[0], zArr[0],
			],[
				xArr[0], yArr[0], zArr[4],
				xArr[4], yArr[0], zArr[4],
			]).span([
				xArr[2], yArr[2], zArr[2],
				xArr[1], yArr[1], zArr[3],
				xArr[3], yArr[1], zArr[3],
				xArr[3], yArr[1], zArr[1],
				xArr[1], yArr[1], zArr[1],
				xArr[1], yArr[1], zArr[3],
			]).span([
				xArr[2], yArr[0], zArr[2],
				xArr[3], yArr[1], zArr[3],
				xArr[1], yArr[1], zArr[3],
				xArr[1], yArr[1], zArr[1],
				xArr[3], yArr[1], zArr[1],
				xArr[3], yArr[1], zArr[3],
			]).build();
			var mesh = new THREE.Mesh(geo, defaultMat);
			return mesh;
		}
	};
})();
