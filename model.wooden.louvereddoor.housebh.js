var makeWoodenLouvereddoorHousebh = function (height, width, callback) {
	/*
	  +----------+
	  |+---++---+|
	  ||===||===||
	  ||===||===||
	  |+---++---+|
	  |+---++---+|
	  ||/ \||/ \||
	  ||\ /||\ /||
	  |+---++---+|
	  +----------+
	 */
	var blueWoodMat = new THREE.MeshPhongMaterial( { color: 0x00868b,
												shininess: 0.1,
												specular: 0x010101, } );
	var group = new THREE.Object3D();
	(function() {
		// frame
		var geo = geoMesh().stripe([
			0.0, 0.0, 0.5,
			0.0, height, 0.5,
			width, height, 0.5,
			width, 0.0, 0.5,
		],[
			0.3, 0.0, 0.5,
			0.3, height-0.3, 0.5,
			width-0.3, height-0.3, 0.5,
			width-0.3, 0.0, 0.5,
		]).stripe([
			0.3, 0.0, 0.0,
			0.3, height-0.3, 0.0,
			width-0.3, height-0.3, 0.0,
			width-0.3, 0.0, 0.0,
		],[
			0.0, 0.0, 0.0,
			0.0, height, 0.0,
			width, height, 0.0,
			width, 0.0, 0.0,
		]).stripe([
			0.3, 0.0, 0.5,
			0.3, height-0.3, 0.5,
			width-0.3, height-0.3, 0.5,
			width-0.3, 0.0, 0.5,
		],[
			0.3, 0.0, 0.0,
			0.3, height-0.3, 0.0,
			width-0.3, height-0.3, 0.0,
			width-0.3, 0.0, 0.0,
		]).build();
		var mesh = new THREE.Mesh(geo, blueWoodMat);
		group.add(mesh);
	})();
	(function() {
		// TODO: door
		var geo = geoMesh().stripe([
			-0.5, 0.5, 0.2,
			-0.5, height-0.8, 0.2,
			1.1-width, height-0.8, 0.2,
			1.1-width, 0.5, 0.2,
			-0.5, 0.5, 0.2,
		],[
			0.0, 0.0, 0.2,
			0.0, height-0.3, 0.2,
			0.6-width, height-0.3, 0.2,
			0.6-width, 0.0, 0.2,
			0.0, 0.0, 0.2,
		]).stripe([
			0.0, 0.0, 0.0,
			0.0, height-0.3, 0.0,
			0.6-width, height-0.3, 0.0,
			0.6-width, 0.0, 0.0,
			0.0, 0.0, 0.0,
		],[
			-0.5, 0.5, 0.0,
			-0.5, height-0.8, 0.0,
			1.1-width, height-0.8, 0.0,
			1.1-width, 0.5, 0.0,
			-0.5, 0.5, 0.0,
		]).stripe([
			0.0, 0.0, 0.2,
			0.0, height-0.3, 0.2,
			0.6-width, height-0.3, 0.2,
			0.6-width, 0.0, 0.2,
			0.0, 0.0, 0.2,
		],[
			0.0, 0.0, 0.0,
			0.0, height-0.3, 0.0,
			0.6-width, height-0.3, 0.0,
			0.6-width, 0.0, 0.0,
			0.0, 0.0, 0.0,
		]).stripe([
			-0.5, 0.5, 0.0,
			-0.5, height-0.8, 0.0,
			1.1-width, height-0.8, 0.0,
			1.1-width, 0.5, 0.0,
			-0.5, 0.5, 0.0,
		],[
			-0.5, 0.5, 0.2,
			-0.5, height-0.8, 0.2,
			1.1-width, height-0.8, 0.2,
			1.1-width, 0.5, 0.2,
			-0.5, 0.5, 0.2,
		]);
		var doorHeight = height - 0.3;
		var doorWidth = width - 2*0.3;
		var doorInnerHeight = doorHeight - 2*0.5;
		var doorInnerWidth = doorWidth - 2*0.5;
		var upperPaneHeight = Math.floor(((doorInnerHeight-0.5)/2.0)/0.3)*0.3; // louver pane height=0.3
		var lowerPaneHeight = doorInnerHeight-upperPaneHeight-0.5;
		var paneWidth = (doorInnerWidth-0.5)/2.0;
		// the middle divider
		geo = geo.stripe([
			-0.5-doorInnerWidth, 0.5+lowerPaneHeight, 0.2,
			-0.5-doorInnerWidth, 1.0+lowerPaneHeight, 0.2,
			-0.5-doorInnerWidth, 1.0+lowerPaneHeight, 0.0,
			-0.5-doorInnerWidth, 0.5+lowerPaneHeight, 0.0,
			-0.5-doorInnerWidth, 0.5+lowerPaneHeight, 0.2,
		],[
			-0.5, 0.5+lowerPaneHeight, 0.2,
			-0.5, 1.0+lowerPaneHeight, 0.2,
			-0.5, 1.0+lowerPaneHeight, 0.0,
			-0.5, 0.5+lowerPaneHeight, 0.0,
			-0.5, 0.5+lowerPaneHeight, 0.2,
		]);
		// lower panes
		geo = geo.stripe([
			-0.5, 0.5, 0.15,
			-0.5-paneWidth, 0.5, 0.15,
			-0.5-paneWidth, 0.5, 0.2,
			-1.0-paneWidth, 0.5, 0.2,
			-1.0-paneWidth, 0.5, 0.15,
			-1.0-2.0*paneWidth, 0.5, 0.15,
		],[
			-0.5, 0.5+lowerPaneHeight, 0.15,
			-0.5-paneWidth, 0.5+lowerPaneHeight, 0.15,
			-0.5-paneWidth, 0.5+lowerPaneHeight, 0.2,
			-1.0-paneWidth, 0.5+lowerPaneHeight, 0.2,
			-1.0-paneWidth, 0.5+lowerPaneHeight, 0.15,
			-1.0-2.0*paneWidth, 0.5+lowerPaneHeight, 0.15,
		]).stripe([
			-0.5, 0.5+lowerPaneHeight, 0.05,
			-0.5-paneWidth, 0.5+lowerPaneHeight, 0.05,
			-0.5-paneWidth, 0.5+lowerPaneHeight, 0.0,
			-1.0-paneWidth, 0.5+lowerPaneHeight, 0.0,
			-1.0-paneWidth, 0.5+lowerPaneHeight, 0.05,
			-1.0-2.0*paneWidth, 0.5+lowerPaneHeight, 0.05,
		],[
			-0.5, 0.5, 0.05,
			-0.5-paneWidth, 0.5, 0.05,
			-0.5-paneWidth, 0.5, 0.0,
			-1.0-paneWidth, 0.5, 0.0,
			-1.0-paneWidth, 0.5, 0.05,
			-1.0-2.0*paneWidth, 0.5, 0.05,
		]);
		// the upper divider
		geo = geo.stripe([
			-0.5-paneWidth, 1.0+lowerPaneHeight, 0.2,
			-1.0-paneWidth, 1.0+lowerPaneHeight, 0.2,
			-1.0-paneWidth, 1.0+lowerPaneHeight, 0.0,
			-0.5-paneWidth, 1.0+lowerPaneHeight, 0.0,
			-0.5-paneWidth, 1.0+lowerPaneHeight, 0.2,
		],[
			-0.5-paneWidth, 1.0+lowerPaneHeight+upperPaneHeight, 0.2,
			-1.0-paneWidth, 1.0+lowerPaneHeight+upperPaneHeight, 0.2,
			-1.0-paneWidth, 1.0+lowerPaneHeight+upperPaneHeight, 0.0,
			-0.5-paneWidth, 1.0+lowerPaneHeight+upperPaneHeight, 0.0,
			-0.5-paneWidth, 1.0+lowerPaneHeight+upperPaneHeight, 0.2,
		]);
		// louver panes
		var numLouver = upperPaneHeight/0.3;
		for (var j=0; j<2; j++) {
			var louverX = -0.5-j*(0.5+paneWidth);
			for (var i=0; i<numLouver; i++) {
				var louverY = 1.0+lowerPaneHeight+i*0.3;
				geo = geo.stripe([
					louverX-paneWidth, louverY, 0.2,
					louverX-paneWidth, louverY+0.05, 0.25,
					louverX-paneWidth, louverY+0.3, 0.0,
					louverX-paneWidth, louverY+0.25, -0.05,
					louverX-paneWidth, louverY, 0.2,
				],[
					louverX, louverY, 0.2,
					louverX, louverY+0.05, 0.25,
					louverX, louverY+0.3, 0.0,
					louverX, louverY+0.25, -0.05,
					louverX, louverY, 0.2,
				]).span([
					louverX, louverY, 0.2,
					louverX, louverY+0.25, -0.05,
					louverX, louverY+0.3, 0.0,
					louverX, louverY+0.05, 0.25,
				]).span([
					louverX-paneWidth, louverY, 0.2,
					louverX-paneWidth, louverY+0.05, 0.25,
					louverX-paneWidth, louverY+0.3, 0.0,
					louverX-paneWidth, louverY+0.25, -0.05,
				]);
			}
		}
		geo = geo.build();
		var mesh = new THREE.Mesh(geo, blueWoodMat);
		mesh.position.x = width-0.3;
		group.add(mesh);
		if (callback) {
			callback(function(angle) {
				mesh.rotation.y = angle;
			});
		}
	})();
	return group;
};
