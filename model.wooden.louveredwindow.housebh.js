var makeWoodenLouveredwindowHousebh = function(height, width, callback) {
	var blueWoodMat = new THREE.MeshPhongMaterial( { color: 0x00868b,
												shininess: 0.1,
												specular: 0x010101, } );
	var blueIronMat = new THREE.MeshPhongMaterial( { color: 0x00868b,
												shininess: 0.5,
												specular: 0x060606, } );
	var group = new THREE.Object3D();
	(function() {
		// frame
		var geo = geoMesh().tube([[
			0.0, 0.0, 0.5,
			0.3, 0.3, 0.5,
			0.3, 0.3, 0.0,
			0.0, 0.0, 0.0,
		],[
			width, 0.0, 0.5,
			width-0.3, 0.3, 0.5,
			width-0.3, 0.3, 0.0,
			width, 0.0, 0.0,
		],[
			width, height, 0.5,
			width-0.3, height-0.3, 0.5,
			width-0.3, height-0.3, 0.0,
			width, height, 0.0,
		],[
			0.0, height, 0.5,
			0.3, height-0.3, 0.5,
			0.3, height-0.3, 0.0,
			0.0, height, 0.0,
		],[
			0.0, 0.0, 0.5,
			0.3, 0.3, 0.5,
			0.3, 0.3, 0.0,
			0.0, 0.0, 0.0,
		]]).build();
		var mesh = new THREE.Mesh(geo, blueWoodMat);
		group.add(mesh);
	})();
	(function() {
		var barPosLeft = width/2.0 - 0.5;
		var barPosRight = width/2.0 + 0.5;
		// two vertical bars
		var geo = geoMesh().stripe([
			barPosLeft, height-0.3, 0.2,
			barPosLeft+0.1, height-0.3, 0.2,
			barPosLeft+0.1, height-0.3, 0.0,
			barPosLeft, height-0.3, 0.0,
			barPosLeft, height-0.3, 0.2,
		],[
			barPosLeft, 0.3, 0.2,
			barPosLeft+0.1, 0.3, 0.2,
			barPosLeft+0.1, 0.3, 0.0,
			barPosLeft, 0.3, 0.0,
			barPosLeft, 0.3, 0.2,
		]).stripe([
			barPosRight, 0.3, 0.2,
			barPosRight-0.1, 0.3, 0.2,
			barPosRight-0.1, 0.3, 0.0,
			barPosRight, 0.3, 0.0,
			barPosRight, 0.3, 0.2,
		],[
			barPosRight, height-0.3, 0.2,
			barPosRight-0.1, height-0.3, 0.2,
			barPosRight-0.1, height-0.3, 0.0,
			barPosRight, height-0.3, 0.0,
			barPosRight, height-0.3, 0.2,
		]);
		// 10 horizontal bars
		var barGap = (height-0.6)/6.0;
		for (var barIdx=0; barIdx<5; barIdx++) {
			var barPos = 0.3+barGap+barIdx*barGap;
			geo = geo.stripe([
				0.3, barPos-0.05, 0.15,
				0.3, barPos+0.05, 0.15,
				0.3, barPos+0.05, 0.05,
				0.3, barPos-0.05, 0.05,
				0.3, barPos-0.05, 0.15,
			],[
				barPosLeft, barPos-0.05, 0.15,
				barPosLeft, barPos+0.05, 0.15,
				barPosLeft, barPos+0.05, 0.05,
				barPosLeft, barPos-0.05, 0.05,
				barPosLeft, barPos-0.05, 0.15,
			]).stripe([
				barPosRight, barPos-0.05, 0.15,
				barPosRight, barPos+0.05, 0.15,
				barPosRight, barPos+0.05, 0.05,
				barPosRight, barPos-0.05, 0.05,
				barPosRight, barPos-0.05, 0.15,
			],[
				width-0.3, barPos-0.05, 0.15,
				width-0.3, barPos+0.05, 0.15,
				width-0.3, barPos+0.05, 0.05,
				width-0.3, barPos-0.05, 0.05,
				width-0.3, barPos-0.05, 0.15,
			]);
		}
		var mesh = new THREE.Mesh(geo.build(), blueIronMat);
		group.add(mesh);
	})();
	(function() {
		// windows
		var windowWidth = (width-0.6)/2.0;
		var windowHeight = height-0.6;
		var numLouver = Math.floor((windowHeight-1.0)/0.3);
		var lowerHeight = windowHeight - 0.5 - numLouver*0.3;
		var geo = geoMesh().tube([[
			0.0, 0.0, 0.2,
			0.5, lowerHeight, 0.2,
			0.5, lowerHeight, 0.0,
			0.0, 0.0, 0.0,
			0.0, 0.0, 0.2,
		],[
			windowWidth, 0.0, 0.2,
			windowWidth-0.5, lowerHeight, 0.2,
			windowWidth-0.5, lowerHeight, 0.0,
			windowWidth, 0.0, 0.0,
			windowWidth, 0.0, 0.2,
		],[
			windowWidth, windowHeight, 0.2,
			windowWidth-0.5, windowHeight-0.5, 0.2,
			windowWidth-0.5, windowHeight-0.5, 0.0,
			windowWidth, windowHeight, 0.0,
			windowWidth, windowHeight, 0.2,
		],[
			0.0, windowHeight, 0.2,
			0.5, windowHeight-0.5, 0.2,
			0.5, windowHeight-0.5, 0.0,
			0.0, windowHeight, 0.0,
			0.0, windowHeight, 0.2,
		],[
			0.0, 0.0, 0.2,
			0.5, lowerHeight, 0.2,
			0.5, lowerHeight, 0.0,
			0.0, 0.0, 0.0,
			0.0, 0.0, 0.2,
		]]);
		// louvers
		var paneWidth = windowWidth - 1.0;
		var louverX = windowWidth - 0.5;
		for (var i=0; i<numLouver; i++) {
			var louverY = lowerHeight+i*0.3;
			geo = geo.stripe([
				louverX-paneWidth, louverY, 0.2,
				louverX-paneWidth, louverY+0.05, 0.25,
				louverX-paneWidth, louverY+0.3, 0.0,
				louverX-paneWidth, louverY+0.25, -0.05,
				louverX-paneWidth, louverY, 0.2,
			],[
				louverX, louverY, 0.2,
				louverX, louverY+0.05, 0.25,
				louverX, louverY+0.3, 0.0,
				louverX, louverY+0.25, -0.05,
				louverX, louverY, 0.2,
			]).span([
				louverX, louverY, 0.2,
				louverX, louverY+0.25, -0.05,
				louverX, louverY+0.3, 0.0,
				louverX, louverY+0.05, 0.25,
			]).span([
				louverX-paneWidth, louverY, 0.2,
				louverX-paneWidth, louverY+0.05, 0.25,
				louverX-paneWidth, louverY+0.3, 0.0,
				louverX-paneWidth, louverY+0.25, -0.05,
			]);
		}
		var meshLeft = new THREE.Mesh(geo.build(), blueWoodMat);
		var meshRight = meshLeft.clone();
		var leftWindow = new THREE.Object3D();
		meshLeft.position.z = -0.2;
		leftWindow.add(meshLeft);
		leftWindow.position.x = 0.3;
		leftWindow.position.y = 0.3;
		leftWindow.position.z = 0.5;
		group.add(leftWindow);
		var rightWindow = new THREE.Object3D();
		meshRight.position.z = -0.2;
		meshRight.position.x = -windowWidth;
		rightWindow.add(meshRight);
		rightWindow.position.x = width-0.3;
		rightWindow.position.y = 0.3;
		rightWindow.position.z = 0.5;
		group.add(rightWindow);
		if (callback) {
			callback(function(angles) {
				leftWindow.rotation.y = angles[0];
				rightWindow.rotation.y = angles[1];
			});
		}
	})();
	return group;
};
