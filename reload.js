var reload = (function () {
	var _self = {};
	var scripts = []; // ['script-name.js', 'script-name.js', 'script-name.js']
	var checkFuncs = []; // ['script name' : function(){}]
	_self.add = (script, checkFunc) => {
		scripts.push(script);
		if (checkFunc)
			checkFuncs[script] = checkFunc;
	};
	_self.reload = (callback) => {
		// load script with timeout and checkFunction
		var recurLoad = function(scriptNo, checkFunc) {
			if (scriptNo >= scripts.length) {
				if (callback)
					callback();
				return;
			}
			if (checkFunc && !checkFunc())
				setTimeout(recurLoad, 10, scriptNo, checkFunc);
			else {
				var script = scripts[scriptNo];
				var oldScript = document.getElementById('id_'+script);
				if (oldScript) {
					oldScript.parentNode.removeChild(oldScript);
				}
				var newScript = document.createElement('script');
				newScript.setAttribute('id', 'id_' + script);
				newScript.setAttribute('src', script);
				document.head.appendChild(newScript);
				var nextCheckFunc = checkFuncs[script];
				setTimeout(recurLoad, 10, scriptNo+1, nextCheckFunc);
			}
		};
		recurLoad(0);
	};
	return _self;
})();
