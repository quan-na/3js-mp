var houseBhScene = (function() {
	var scene = new THREE.Scene();
	var group = new THREE.Object3D();
	var concreteMat = new THREE.MeshPhongMaterial( { color: 0x686c5e,
												shininess: 10,
												specular: 0x111111, } );
	var yellowWallMat = new THREE.MeshPhongMaterial( { color: 0xF0E68C,
												shininess: 0.1,
												specular: 0x111111, } );
	var tileTexture = loader.load('seamless_terracotta_tiles_texture.jpg');
	tileTexture.wrapS = THREE.RepeatWrapping;
	tileTexture.wrapT = THREE.RepeatWrapping;
	var yardTileMat = new THREE.MeshPhongMaterial( { map: tileTexture,
												shininess: 5.1,
												specular: 0x111111, } );
	(function () {
		var pointLight = new THREE.PointLight( 0xffffff, 1.5, 70 );
		pointLight.position.x = pointLight.position.z = 25;
		pointLight.position.y = 30;
		group.add(pointLight);
		var ambientLight = new THREE.AmbientLight( 0xffffff, 0.4 );
		group.add(ambientLight);
	})();
	(function () {
		// verada bottom
		var geo = geoMesh().stripe([ // the big block
			1.0, 0.0, 7.0,
			1.0, 2.4, 7.0,
			1.0, 2.4, 0.0,
		], [
			29.0, 0.0, 7.0,
			29.0, 2.4, 7.0,
			29.0, 2.4, 0.0,
		]).stripe([ // slope
			22.0, 0.0, 11.8,
			22.0, 2.4, 7.0,
		],[
			26.0, 0.0, 11.8,
			26.0, 2.4, 7.0,
		]).position([
			22.0, 0.0, 7.0,
			22.0, 0.0, 11.8,
			22.0, 2.4, 7.0,
		]).position([
			26.0, 0.0, 7.0,
			26.0, 2.4, 7.0,
			26.0, 0.0, 11.8,
		]).span([ // left step
			19.0, 1.2, 9.2,
			19.0, 0.0, 9.2,
			22.0, 0.0, 9.2,
			22.0, 1.2, 9.2,
			22.0, 1.2, 7.0,
			19.0, 1.2, 7.0,
			19.0, 0.0, 7.0,
			19.0, 0.0, 9.2,
		]).span([ // right step
			29.0, 1.2, 9.2,
			29.0, 0.0, 9.2,
			29.0, 0.0, 7.0,
			29.0, 1.2, 7.0,
			26.0, 1.2, 7.0,
			26.0, 1.2, 9.2,
			26.0, 0.0, 9.2,
			29.0, 0.0, 9.2,
		]).stripe([
			19.0, 2.4, 0.0,
			19.0, 2.5, 0.0,
			19.0, 2.5, -1.0,
		],[
			27.0, 2.4, 0.0,
			27.0, 2.5, 0.0,
			27.0, 2.5, -1.0,
		]).build();
		var mesh = new THREE.Mesh(geo, concreteMat);
		group.add(mesh);
	})();
	(function () {
		// verada top
		var geo = geoMesh().stripe([
			1.0, 21.0, 0.0,
			1.0, 21.0, 7.2,
			1.0, 22.7, 7.2,
			1.0, 22.7, 7.0,
			1.0, 22.5, 7.0,
			1.0, 22.5, 0.0,
		], [
			29.0, 21.0, 0.0,
			29.0, 21.0, 7.2,
			29.0, 22.7, 7.2,
			29.0, 22.7, 7.0,
			29.0, 22.5, 7.0,
			29.0, 22.5, 0.0,
		]).stripe([
			5.0, 24.5, 0.0,
			25.0, 24.5, 0.0,
			25.0, 27.5, 0.0,
			5.0, 27.5, 0.0,
			5.0, 24.5, 0.0,
		],[
			0.0, 22.5, 0.0,
			30.0, 22.5, 0.0,
			30.0, 29.5, 0.0,
			0.0, 29.5, 0.0,
			0.0, 22.5, 0.0,
		]).build();
		var mesh = new THREE.Mesh(geo, yellowWallMat);
		group.add(mesh);
		var abLeft = makeConcreteAirbrickHousebh(10.0, 3.0);
		abLeft.position.x = 5.0;
		abLeft.position.y = 24.5;
		abLeft.position.z = -0.4;
		var abRight = makeConcreteAirbrickHousebh(10.0, 3.0);
		abRight.position.x = 15.0;
		abRight.position.y = 24.5;
		abRight.position.z = -0.4;
		group.add(abLeft);
		group.add(abRight);
	})();
	(function () {
		// verada middle x : 28 4 + w10 + 4 + d8 + 2
		var geo = geoMesh().stripe([
			27.0, 2.4, 0.0,
			27.0, 17.5, 0.0,
			4.0, 17.5, 0.0,
			4.0, 9.5, 0.0,
			14.0, 9.5, 0.0,
			14.0, 17.5, 0.0,
		], [
			29.0, 2.4, 0.0,
			29.0, 18.5, 0.0,
			1.0, 18.5, 0.0,
			1.0, 2.4, 0.0,
			19.0, 2.4, 0.0,
			19.0, 17.5, 0.0,
		]).stripe([
			1.0, 18.5, 0.0,
			1.0, 21.0, 0.0,
			29.0, 21.0, 0.0,
			29.0, 18.5, 0.0,
		],[
			6.0, 18.5, 0.0,
			6.0, 20.5, 0.0,
			24.0, 20.5, 0.0,
			24.0, 18.5, 0.0,
		]).build();
		var mesh = new THREE.Mesh(geo, yellowWallMat);
		group.add(mesh);
		for(var idx=0; idx<9; idx++) {
			var airbrick = makeClayAirbrickHousebh();
			airbrick.position.x = 6.0 + idx*2.0;
			airbrick.position.y = 18.5;
			airbrick.position.z = -0.5;
			group.add(airbrick);
		}
		var door = makeWoodenLouvereddoorHousebh(15.0, 8.0, function(updater) {
			var doorAnimate = objectAnimate([0.0], function(state) {
				updater(state[0]);
				updateFlag = true;
			});
			var closed = true;
			updaters.push(function(scene, actionIndex) {
				if (scene=='0(0)0' && actionIndex==1) {
					if (closed)
						doorAnimate.tween([-Math.PI/2], 50, 2000);
					else
						doorAnimate.tween([0.0], 60, 3000);
					closed = !closed;
				}
			});
		});
		door.position.x = 19.0;
		door.position.y = 2.5;
		door.position.z = -0.5;
		group.add(door);
		var windows = makeWoodenLouveredwindowHousebh(8.0, 10.0, function(updater) {
			// events
			var windowAnimate = objectAnimate([0.0, 0.0], function(state) {
				updater(state);
				updateFlag = true;
			});
			var closed = true;
			updaters.push(function(scene, actionIndex) {
				if (scene=='0(0)0' && actionIndex==2) {
					if (closed)
						windowAnimate.tween([-1.47*Math.PI/2.0, Math.PI], 50, 4000);
					else
						windowAnimate.tween([0.0, 0.0], 60, 5000);
					closed = !closed;
				}
			});
		});
		windows.position.x = 4.0;
		windows.position.y = 9.5;
		windows.position.z = -0.5;
		group.add(windows);
	})();
	(function () {
		// verada left
		var geo = geoMesh().stripe([
			0.0, 0.0, 7.2,
			0.0, 26.0, 7.2,
			0.0, 26.0, 0.0,
		],[
			1.0, 0.0, 7.2,
			1.0, 26.0, 7.2,
			1.0, 26.0, 0.0,
		]).stripe([
			1.0, 8.5, 6.0,
			1.0, 8.5, 2.0,
			1.0, 16.5, 2.0,
			1.0, 16.5, 6.0,
			1.0, 8.5, 6.0,
		],[
			1.0, 0.0, 7.2,
			1.0, 0.0, 0.0,
			1.0, 26.0, 0.0,
			1.0, 26.0, 7.2,
			1.0, 0.0, 7.2,
		]).build();
		var mesh = new THREE.Mesh(geo, yellowWallMat);
		group.add(mesh);
		for (var i=0; i<2; i++)
			for (var j=0; j<4; j++) {
				var airbrick = makeClayAirbrickHousebh();
				airbrick.position.x = 0.5;
				airbrick.position.y = 8.5+j*2.0;
				airbrick.position.z = 6.0-i*2.0;
				airbrick.rotation.y = Math.PI / 2;
				group.add(airbrick);
			}
	})();
	(function () {
		// verada right
		var geo = geoMesh().stripe([
			29.0, 0.0, 7.2,
			29.0, 26.0, 7.2,
			29.0, 26.0, 0.0,
		],[
			30.0, 0.0, 7.2,
			30.0, 26.0, 7.2,
			30.0, 26.0, 0.0,
		]).stripe([
			29.0, 8.5, 6.0,
			29.0, 16.5, 6.0,
			29.0, 16.5, 2.0,
			29.0, 8.5, 2.0,
			29.0, 8.5, 6.0,
		],[
			29.0, 0.0, 7.2,
			29.0, 26.0, 7.2,
			29.0, 26.0, 0.0,
			29.0, 0.0, 0.0,
			29.0, 0.0, 7.2,
		]).build();
		var mesh = new THREE.Mesh(geo, yellowWallMat);
		group.add(mesh);
		for (var i=0; i<2; i++)
			for (var j=0; j<4; j++) {
				var airbrick = makeClayAirbrickHousebh();
				airbrick.position.x = 29.0;
				airbrick.position.y = 8.5+j*2.0;
				airbrick.position.z = 6.0-i*2.0;
				airbrick.rotation.y = Math.PI / 2;
				group.add(airbrick);
			}
	})();
	(function (x0, x1, x2, y0, z0, z1, z2, z3) {
		// the front yard
		var geo = geoMesh().stripe([
			x1, y0, z3,
			x1, y0, z0,
		],[
			x2, y0, z3,
			x2, y0, z0,
		],[
			(z3-z0)/8.0, x1/8.0,
			0.0, x1/8.0,
		],[
			(z3-z0)/8.0, x2/8.0,
			0.0, x2/8.0,
		]).stripe([
			x0, y0, z2,
			x0, y0, z1,
		],[
			x1, y0, z2,
			x1, y0, z1,
		],[
			(z2-z0)/8.0, 0.0,
			(z1-z0)/8.0, 0.0,
		],[
			(z2-z0)/8.0, 1.0,
			(z1-z0)/8.0, 1.0,
		]).build();
		var mesh = new THREE.Mesh(geo, yardTileMat);
		group.add(mesh);
		var slapGeo = geoMesh().tube([[
			x0, y0, z1,
			x0, y0+0.5, z1,
			x0, y0+0.5, z1-0.5,
			x0, y0, z1-0.5,
		],[
			x1, y0, z1,
			x1, y0+0.5, z1,
			x1-0.5, y0+0.5, z1-0.5,
			x1-0.5, y0, z1-0.5,
		],[
			x1, y0, z0,
			x1, y0+0.5, z0,
			x1-0.5, y0+0.5, z0,
			x1-0.5, y0, z0,
		]]).tube([[
			x1, y0, z3-1.0,
			x1, y0+0.5, z3-1.0,
			x1-0.5, y0+0.5, z3-1.0,
			x1-0.5, y0, z3-1.0,
		],[
			x1, y0, z2,
			x1, y0+0.5, z2,
			x1-0.5, y0+0.5, z2+0.5,
			x1-0.5, y0, z2+0.5,
		],[
			x0, y0, z2,
			x0, y0+0.5, z2,
			x0, y0+0.5, z2+0.5,
			x0, y0, z2+0.5,
		]]);
		var slapMesh = new THREE.Mesh(slapGeo.build(), concreteMat);
		group.add(slapMesh);
	})(0.0, 8.0, 30.0, 0.0, 7.0, 15.0, 22.0, 30.0);
	(function(leftGateX, columnSize, columnHeightY, columnTopDelta
		, wallHeightY, wallTopY, wallThickZ) {
		// gate and front wall
		var geo = geoMesh().stripe([
			30.0, columnHeightY, 30.0-columnSize,
			30.0-columnSize, columnHeightY, 30.0-columnSize,
			30.0-columnSize, columnHeightY, 30.0,
		],[
			30.0, 0.0, 30.0-columnSize,
			30.0-columnSize, 0.0, 30.0-columnSize,
			30.0-columnSize, 0.0, 30.0,
		]).stripe([
			30.0, columnHeightY, 30.0-columnSize-columnTopDelta,
			30.0-columnSize-columnTopDelta, columnHeightY, 30.0-columnSize-columnTopDelta,
		],[
			30.0, columnHeightY, 30,
			30.0-columnSize-columnTopDelta, columnHeightY, 30,
		]).span([
			30.0-columnSize-columnTopDelta, columnHeightY+wallTopY, 30-columnSize-columnTopDelta,
			30.0-columnSize-columnTopDelta, columnHeightY, 30-columnSize-columnTopDelta,
			30.0-columnSize-columnTopDelta, columnHeightY, 30,
			30.0-columnSize-columnTopDelta, columnHeightY+wallTopY, 30,
			30.0, columnHeightY+wallTopY, 30,
			30.0, columnHeightY+wallTopY, 30-columnSize-columnTopDelta,
			30.0, columnHeightY, 30-columnSize-columnTopDelta,
			30.0-columnSize-columnTopDelta, columnHeightY, 30-columnSize-columnTopDelta,
		]).stripe([
			leftGateX, columnHeightY, 30.0,
			leftGateX, columnHeightY, 30.0-columnSize,
			leftGateX-columnSize, columnHeightY, 30.0-columnSize,
			leftGateX-columnSize, columnHeightY, 30.0,
		],[
			leftGateX, 0.0, 30.0,
			leftGateX, 0.0, 30.0-columnSize,
			leftGateX-columnSize, 0.0, 30.0-columnSize,
			leftGateX-columnSize, 0.0, 30.0,
		]).stripe([
			leftGateX+columnTopDelta, columnHeightY+wallTopY, 30.0-columnSize-columnTopDelta,
			leftGateX+columnTopDelta, columnHeightY, 30.0-columnSize-columnTopDelta,
			leftGateX-columnSize-columnTopDelta, columnHeightY, 30.0-columnSize-columnTopDelta,
		],[
			leftGateX+columnTopDelta, columnHeightY+wallTopY, 30,
			leftGateX+columnTopDelta, columnHeightY, 30,
			leftGateX-columnSize-columnTopDelta, columnHeightY, 30,
		]).span([
			leftGateX-columnSize-columnTopDelta, columnHeightY+wallTopY, 30-columnSize-columnTopDelta,
			leftGateX-columnSize-columnTopDelta, columnHeightY, 30-columnSize-columnTopDelta,
			leftGateX-columnSize-columnTopDelta, columnHeightY, 30,
			leftGateX-columnSize-columnTopDelta, columnHeightY+wallTopY, 30,
			leftGateX+columnTopDelta, columnHeightY+wallTopY, 30,
			leftGateX+columnTopDelta, columnHeightY+wallTopY, 30-columnSize-columnTopDelta,
			leftGateX+columnTopDelta, columnHeightY, 30-columnSize-columnTopDelta,
			leftGateX-columnSize-columnTopDelta, columnHeightY, 30-columnSize-columnTopDelta,
		]).stripe([
			0.0, 0.0, 30.0-wallThickZ,
			leftGateX-columnSize, 0.0, 30.0-wallThickZ,
			leftGateX-columnSize, wallHeightY, 30.0-wallThickZ,
			0.0, wallHeightY, 30.0-wallThickZ,
			0.0, 0.0, 30.0-wallThickZ,
		],[
			4.0, 2.0, 30.0-wallThickZ,
			14.0, 2.0, 30.0-wallThickZ,
			14.0, 6.0, 30.0-wallThickZ,
			4.0, 6.0, 30.0-wallThickZ,
			4.0, 2.0, 30.0-wallThickZ,
		]).stripe([
			leftGateX-columnSize, wallHeightY, 30.0-wallThickZ,
			leftGateX-columnSize, wallHeightY, 30.0-wallThickZ-columnTopDelta,
			leftGateX-columnSize, wallHeightY+wallTopY, 30.0-wallThickZ-columnTopDelta,
			leftGateX-columnSize, wallHeightY+wallTopY, 30.0,
		],[
			0.0, wallHeightY, 30.0-wallThickZ,
			0.0, wallHeightY, 30.0-wallThickZ-columnTopDelta,
			0.0, wallHeightY+wallTopY, 30.0-wallThickZ-columnTopDelta,
			0.0, wallHeightY+wallTopY, 30.0,
		]).build();
		var mesh = new THREE.Mesh(geo, yellowWallMat);
		group.add(mesh);
		for (var i=0; i<2; i++)
			for (var j=0; j<5; j++) {
				var airbrick = makeClayAirbrickHousebh();
				airbrick.position.x = 4.0+j*2.0;
				airbrick.position.y = 2.0+i*2.0;
				airbrick.position.z = 30.0-wallThickZ;
				group.add(airbrick);
			}
	})(19.0, 2.0, 10.5, 0.3
		, 8.0, 0.5, 1.0);
	scene.add(group);
	scenes['0(0)0'] = scene;
	updateFlag = true;
	return scene;
})();
