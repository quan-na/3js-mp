/* try out the buffered geometry */
var geometry = geoMesh().position([
	-1.0, -1.0,  1.0,
	 1.0, -1.0,  1.0,
	 1.0,  1.0,  1.0,
]).position([
	 1.0,  2.0,  1.0,
	-1.0,  1.0,  1.0,
	-1.0, -1.0,  1.0
]).build();
var material = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
var mesh = new THREE.Mesh( geometry, material );

scene.add( mesh );
