(function() {
	// TODO: implement
	var scene = houseBhScene;
	var group = new THREE.Object3D();
	var concreteMat = new THREE.MeshPhongMaterial( { color: 0x686c5e,
												shininess: 10,
												specular: 0x111111, } );
	(function() {
		// floor
		var geo = geoMesh().span([
			0.7, 2.5, 0.0,
			0.7, 2.5, 29.0,
			29.3, 2.5, 29.0,
			29.3, 2.5, 0.0,
		]);
		var mesh = new THREE.Mesh(geo.build(), concreteMat);
		group.add(mesh);
	})();
	(function() {
		// TODO: west wall
		// z : 0 --> 29, x : 0.7, y : 2.5 --> 29.5->22.5
	})();
	(function() {
		// TODO: east wall
	})();
	(function() {
		// TODO: south wall
	})();
	(function() {
		// TODO: north wall
	})();
	group.position.z = -30.0;
	scene.add(group);
	scenes['0(0)-1'] = scene;
	updateFlag = true;
})();
