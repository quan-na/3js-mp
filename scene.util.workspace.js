scene = typeof scene == 'undefined' ? {} : scene;
scene.util = typeof scene.util == 'undefined' ? {} : scene.util;
scene.util.workspace = (function() {
	// this scene does not have specific coordinate
	// it's a place to create models for other scenes
	var coord = {x:-1, y:0, z:0};
	return scenes[coord.x+':'+coord.y+':'+coord.z] = function(drawType, cs) {
		if (['x','y','z'].indexOf(drawType) != -1) {
			if (drawType == 'z') {
			} else if (drawType == 'x') {
			} else { // y
			}
			return cs;
		} else {
			var group = new THREE.Object3D();
			return group;
		}
	};
})();
