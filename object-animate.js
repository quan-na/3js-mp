var Easing = {
	linear : (percTime, start, end) => start + percTime * (end-start),
};
var objectAnimate = function(initState, updater) {
	var sync = 0;
	return {
		tween : function(toState, steps, time, easingFunc) {
			sync = (sync+1)%1000;
			var currSync = sync;
			var fromState = initState;
			if (!easingFunc)
				easingFunc = Easing.linear;
			var loop = function(step) {
				if (currSync != sync || step > steps) {
					updater(initState, true); // flag end of animation
					return;
				}
				// update
				if (step == steps) {
					initState = toState;
					updater(initState, true);
					return;
				}
				// step < steps
				var percTime = step/steps;
				var changed = false;
				for (var i=0; i<initState.length; i++) {
					initState[i] = easingFunc(percTime, fromState[i], toState[i]);
				}
				updater(initState);
				// call recur
				setTimeout(loop, time/steps, step+1);
			};
			setTimeout(loop, time/steps, 1);
		},
		stop : function() {
			sync = (sync+1)%1000;
		},
	};
};
