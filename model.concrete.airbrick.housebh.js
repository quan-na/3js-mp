var makeConcreteAirbrickHousebh = function(width, height) {
	var concreteMat = new THREE.MeshPhongMaterial( { color: 0xC0C0C0,
												shininess: 0.1,
												specular: 0x010101, } );
	var geo = geoMesh().tube([[
		0.3, 0.3, 0.5,
		0.3, 0.3, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.5,
		0.3, 0.3, 0.5,
	],[
		width-0.3, 0.3, 0.5,
		width-0.3, 0.3, 0.0,
		width, 0.0, 0.0,
		width, 0.0, 0.5,
		width-0.3, 0.3, 0.5,
	],[
		width-0.3, height-0.3, 0.5,
		width-0.3, height-0.3, 0.0,
		width, height, 0.0,
		width, height, 0.5,
		width-0.3, height-0.3, 0.5,
	],[
		0.3, height-0.3, 0.5,
		0.3, height-0.3, 0.0,
		0.0, height, 0.0,
		0.0, height, 0.5,
		0.3, height-0.3, 0.5,
	],[
		0.3, 0.3, 0.5,
		0.3, 0.3, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.5,
		0.3, 0.3, 0.5,
	]]);
	var louverHeight = (height-0.6)/3.0;
	for (var idx=0; idx<3; idx++) {
		var louverY = 0.3 + idx*louverHeight;
		geo = geo.stripe([
			0.3, louverY, 0.5,
			0.3, louverY+0.3, 0.5,
			0.3, louverY+louverHeight, 0.0,
			0.3, louverY+louverHeight-0.3, 0.0,
			0.3, louverY, 0.5,
		],[
			width-0.3, louverY, 0.5,
			width-0.3, louverY+0.3, 0.5,
			width-0.3, louverY+louverHeight, 0.0,
			width-0.3, louverY+louverHeight-0.3, 0.0,
			width-0.3, louverY, 0.5,
		]);
	}
	var mesh = new THREE.Mesh(geo.build(), concreteMat);
	return mesh;
};
