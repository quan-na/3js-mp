/* a test module with 1 cube */
var geometry = new THREE.BoxGeometry( 0.1, 0.1, 0.1 );
var mat0 = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
var mat1 = new THREE.MeshBasicMaterial( { color: 0xff0000 } );
var mat2 = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var mat3 = new THREE.MeshBasicMaterial( { color: 0x0000ff } );
var cube0 = new THREE.Mesh( geometry, mat0 );
var cube1 = new THREE.Mesh( geometry, mat1 );
var cube2 = new THREE.Mesh( geometry, mat2 );
var cube3 = new THREE.Mesh( geometry, mat3 );
scene.add( cube0 );
scene.add( cube1 );
scene.add( cube2 );
scene.add( cube3 );
cube1.position.z = -10;
cube2.position.x = 10;
cube3.position.z = -10;
cube3.position.x = 10;

updaters.push(function() {
	cube0.rotation.y += 0.1;
	cube1.rotation.y += 0.1;
	cube2.rotation.y += 0.1;
	cube3.rotation.y += 0.1;
});
