scene = typeof scene == 'undefined' ? {} : scene;
scene.oldhomebh = typeof scene.oldhomebh == 'undefined' ? {} : scene.oldhomebh;
scene.oldhomebh.frontyard = (function() {
	var verada = {
		x : { min:0.0, side1:0.1, window0:0.4, window1:1.4, door0:1.9, door1:2.6, side2:2.9, max:3.0, step0:1.7, step1:2.1, step2:2.5, ubrick1:1.5 },
		y : { min:0.0, step1:0.2, door0:0.4, window0:1.0, door1:2.0, brick0:2.1, upper0:2.3, upper1:2.5, ubrick0:2.6, side1:2.75, ubrick1:2.9, max:3.0 },
		z : { min:0.0, wall1:0.2, brick0:0.4, brick1:0.8, side1:1.0, step1:1.3, step2:1.7 },
	};
	if (typeof THREE != 'undefined') {
		var concreteMat = new THREE.MeshPhongMaterial( { color: 0x686c5e,
														 shininess: 0.1,
														 specular: 0x111111, } );
		var yellowWallMat = new THREE.MeshPhongMaterial( { color: 0xF0E68C,
														   shininess: 0.1,
														   specular: 0x111111, } );
		var tileTexture = loader.load('seamless_terracotta_tiles_texture.jpg');
		tileTexture.wrapS = THREE.RepeatWrapping;
		tileTexture.wrapT = THREE.RepeatWrapping;
		var yardTileMat = new THREE.MeshPhongMaterial( { map: tileTexture,
														 shininess: 5.1,
														 specular: 0x111111, } );
	}
	return scenes['0:0:0'] = function(drawType, cs) {
		if (['x','y','z'].indexOf(drawType) != -1) {
			var ver = {};
			[ver.x, ver.y] = bpAxesConvert(drawType, verada.x, verada.y, verada.z);
			if (drawType == 'z') {
				cs.strokeRect(ver.x.min, ver.y.max, ver.x.max-ver.x.min, ver.y.min-ver.y.max); // wall
				cs.strokeRect(ver.x.min, ver.y.side1, ver.x.side1-ver.x.min, ver.y.min-ver.y.side1); // side wall 0
				cs.strokeRect(ver.x.side2, ver.y.side1, ver.x.max-ver.x.side2, ver.y.min-ver.y.side1); // side wall 0
				cs.strokeRect(ver.x.side1, ver.y.upper1, ver.x.side2-ver.x.side1, ver.y.upper0-ver.y.upper1); // upper block
				cs.strokeRect(ver.x.side1, ver.y.door0, ver.x.side2-ver.x.side1, ver.y.min-ver.y.door0); // lower block
				cs.strokeRect(ver.x.window0, ver.y.door1, ver.x.window1-ver.x.window0, ver.y.window0-ver.y.door1); // window
				cs.strokeRect(ver.x.door0, ver.y.door1, ver.x.door1-ver.x.door0, ver.y.door0-ver.y.door1); // door
				cs.strokeRect(ver.x.window0, ver.y.upper0, ver.x.door1-ver.x.window0, ver.y.brick0-ver.y.upper0); // brick
				cs.strokeRect(ver.x.window0, ver.y.ubrick1, ver.x.door1-ver.x.window0, ver.y.ubrick0-ver.y.ubrick1); // ubrick
				cs.strokeRect(ver.x.step0, ver.y.step1, ver.x.step1-ver.x.step0, ver.y.min-ver.y.step1); // step0
				cs.strokeRect(ver.x.step1, ver.y.door0, ver.x.step2-ver.x.step1, ver.y.min-ver.y.door0); // step1
				cs.strokeRect(ver.x.step2, ver.y.step1, ver.x.side2-ver.x.step2, ver.y.min-ver.y.step1); // step0
			} else if (drawType == 'x') {
				cs.strokeRect(ver.x.side1, ver.y.door0, ver.x.wall1-ver.x.side1, ver.y.min-ver.y.door0); // lower block
				cs.strokeRect(ver.x.wall1, ver.y.max, ver.x.min-ver.x.wall1, ver.y.min-ver.y.max); // wall
				cs.strokeRect(ver.x.side1, ver.y.side1, ver.x.wall1-ver.x.side1, ver.y.min-ver.y.side1); // side wall
				cs.strokeRect(ver.x.wall1, ver.y.door1, ver.x.min-ver.x.wall1, ver.y.door0-ver.y.door1); // door
				cs.strokeRect(ver.x.wall1, ver.y.door1, ver.x.min-ver.x.wall1, ver.y.window0-ver.y.door1); // window
				cs.strokeRect(ver.x.side1, ver.y.upper0, ver.x.wall1-ver.x.side1, ver.y.upper1-ver.y.upper0); // upper block
				cs.strokeRect(ver.x.wall1, ver.y.ubrick1, ver.x.min-ver.x.wall1, ver.y.ubrick0-ver.y.ubrick1); // upper air brick
				cs.strokeRect(ver.x.wall1, ver.y.upper0, ver.x.min-ver.x.wall1, ver.y.brick0-ver.y.upper0); // air brick
				cs.strokeRect(ver.x.brick1, ver.y.door1, ver.x.brick0-ver.x.brick1, ver.y.window0-ver.y.door1); // side air brick
				cs.beginPath();
				cs.moveTo(ver.x.side1, ver.y.min);
				cs.lineTo(ver.x.step2, ver.y.min);
				cs.lineTo(ver.x.side1, ver.y.door0);
				cs.moveTo(ver.x.step1, ver.y.min);
				cs.lineTo(ver.x.step1, ver.y.step1);
				cs.lineTo(ver.x.side1, ver.y.step1);
				cs.stroke();
			} else { // y
				cs.strokeRect(ver.x.min, ver.y.min, ver.x.max-ver.x.min, ver.y.wall1-ver.y.min); // wall
				cs.strokeRect(ver.x.window0, ver.y.min, ver.x.window1-ver.x.window0, ver.y.wall1-ver.y.min); // window
				cs.strokeRect(ver.x.door0, ver.y.min, ver.x.door1-ver.x.door0, ver.y.wall1-ver.y.min); // door
				cs.strokeRect(ver.x.min, ver.y.wall1, ver.x.side1-ver.x.min, ver.y.side1-ver.y.wall1); // side wall 0
				cs.strokeRect(ver.x.side2, ver.y.wall1, ver.x.max-ver.x.side2, ver.y.side1-ver.y.wall1); // side wall 1
				cs.strokeRect(ver.x.side1, ver.y.wall1, ver.x.side2-ver.x.side1, ver.y.side1-ver.y.wall1); // upper & lower block
				cs.strokeRect(ver.x.step0, ver.y.side1, ver.x.step1-ver.x.step0, ver.y.step1-ver.y.side1); // step0
				cs.strokeRect(ver.x.step1, ver.y.side1, ver.x.step2-ver.x.step1, ver.y.step2-ver.y.side1); // step1
				cs.strokeRect(ver.x.step2, ver.y.side1, ver.x.side2-ver.x.step2, ver.y.step1-ver.y.side1); // step2
			}
			return cs;
		} else {
			var group = new THREE.Object3D();
			(function() { // verada bottom
				var geo = geoMesh().box(
					verada.x.side1, verada.x.side2, verada.y.min, verada.y.door0, verada.z.min, verada.z.side1,
				).cylynder([
					verada.x.step1, verada.y.door0, verada.z.side1,
					verada.x.step1, verada.y.min, verada.z.side1,
					verada.x.step1, verada.y.min, verada.z.step2,
				],[
					verada.x.step2, verada.y.door0, verada.z.side1,
					verada.x.step2, verada.y.min, verada.z.side1,
					verada.x.step2, verada.y.min, verada.z.step2,
				]).box(
					verada.x.step0, verada.x.step1, verada.y.min, verada.y.step1, verada.z.side1, verada.z.step1,
				).box(
					verada.x.step2, verada.x.side2, verada.y.min, verada.y.step1, verada.z.side1, verada.z.step1,
				).build();
				var mesh = new THREE.Mesh(geo, concreteMat);
				group.add(mesh);
			})();
			(function() { // verada walls
				var geo = geoMesh().box(
					verada.x.min, verada.x.side1, verada.y.min, verada.y.door0, verada.z.min, verada.z.side1,
				).box(
					verada.x.side2, verada.x.max, verada.y.min, verada.y.door0, verada.z.min, verada.z.side1,
				).box(
					verada.x.min, verada.x.window0, verada.y.door0, verada.y.max, verada.z.min, verada.z.wall1,
				).box(
					verada.x.door1, verada.x.max, verada.y.door0, verada.y.max, verada.z.min, verada.z.wall1,
				).box(
					verada.x.window0, verada.x.door1, verada.y.door1, verada.y.brick0, verada.z.min, verada.z.wall1,
				).box(
					verada.x.window0, verada.x.door1, verada.y.upper0, verada.y.ubrick0, verada.z.min, verada.z.wall1,
				).box(
					verada.x.window0, verada.x.door1, verada.y.ubrick1, verada.y.max, verada.z.min, verada.z.wall1,
				).box(
					verada.x.window1, verada.x.door0, verada.y.door0, verada.y.door1, verada.z.min, verada.z.wall1,
				).box(
					verada.x.window0, verada.x.window1, verada.y.door0, verada.y.window0, verada.z.min, verada.z.wall1,
				).box(
					verada.x.min, verada.x.side1, verada.y.door0, verada.y.side1, verada.z.wall1, verada.z.brick0,
				).box(
					verada.x.min, verada.x.side1, verada.y.door0, verada.y.side1, verada.z.brick1, verada.z.side1,
				).box(
					verada.x.min, verada.x.side1, verada.y.door0, verada.y.window0, verada.z.brick0, verada.z.brick1,
				).box(
					verada.x.min, verada.x.side1, verada.y.door1, verada.y.side1, verada.z.brick0, verada.z.brick1,
				).box(
					verada.x.side2, verada.x.max, verada.y.door0, verada.y.side1, verada.z.wall1, verada.z.brick0,
				).box(
					verada.x.side2, verada.x.max, verada.y.door0, verada.y.side1, verada.z.brick1, verada.z.side1,
				).box(
					verada.x.side2, verada.x.max, verada.y.door0, verada.y.window0, verada.z.brick0, verada.z.brick1,
				).box(
					verada.x.side2, verada.x.max, verada.y.door1, verada.y.side1, verada.z.brick0, verada.z.brick1,
				).box(
					verada.x.side1, verada.x.side2, verada.y.upper0, verada.y.upper1, verada.z.wall1, verada.z.side1,
				).build();
				var mesh = new THREE.Mesh(geo, yellowWallMat);
				group.add(mesh);
			})();
			return group;
		}
	};
})();
