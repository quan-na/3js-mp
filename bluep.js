var zoomFactor = 1;
function bpAxesConvert(viewType, xArr, yArr, zArr) {
	if (Array.isArray(xArr) && Array.isArray(yArr) && Array.isArray(zArr)) {
		if (viewType == 'z') {
			return [xArr.map((x) => 100*x*zoomFactor), yArr.map((y) => (300-100*y)*zoomFactor)];
		} else if (viewType == 'x') {
			return [zArr.map((z) => (300-100*z)*zoomFactor), yArr.map((y) => (300-100*y)*zoomFactor)];
		} else { // y
			return [xArr.map((x) => 100*x*zoomFactor), zArr.map((z) => 100*z*zoomFactor)];
		}
	} else {
		switch (viewType) {
		case 'z':
			return [Object.keys(xArr).reduce(function(obj, key) {obj[key] = 100*xArr[key]*zoomFactor; return obj;}, {}), Object.keys(yArr).reduce(function(obj, key) {obj[key] = (300-100*yArr[key])*zoomFactor; return obj;}, {})];
		case 'x':
			return [Object.keys(zArr).reduce(function(obj, key) {obj[key] = (300-100*zArr[key])*zoomFactor; return obj;}, {}), Object.keys(yArr).reduce(function(obj, key) {obj[key] = (300-100*yArr[key])*zoomFactor; return obj;}, {})];
		default:
			return [Object.keys(xArr).reduce(function(obj, key) {obj[key] = 100*xArr[key]*zoomFactor; return obj;}, {}), Object.keys(zArr).reduce(function(obj, key) {obj[key] = 100*zArr[key]*zoomFactor; return obj;}, {})];
		}
	}
}

var scenes = [];
(function() {
	var canvas = document.getElementById('bpCanvas');
	var ctx = canvas.getContext('2d');
	var pos = {x:0, y:0, z:0, view:'y'};
	/*
	  The axes and params should be converted to 2d before drawing
	  - 3.0 in 3js => 300 in blue print
	  - default view is y top-down: x*100->x z*100->y
	  - z front view: x*100->x 300-y*100->y
	  - x right view: 300-z*100->x 300-y*100->y
	*/
	function reDraw() {
		ctx.strokeStyle = 'white';
		ctx.fillStyle = 'rgba(0, 0, 200, 0.5)';
		ctx.clearRect(0, 0, 300*zoomFactor, 300*zoomFactor);
		var sceneCode = pos.x+':'+pos.y+':'+pos.z;
		var detailEle = document.getElementById('detail');
		if (typeof scenes[sceneCode] == 'function') {
			scenes[sceneCode](pos.view, ctx);
			detailEle.innerText = 'scene: '+sceneCode+' view:'+pos.view;
		} else {
			scene.default(pos.view, ctx);
			detailEle.innerText = 'default: '+sceneCode+' view:'+pos.view;
		}
	}
	reload.reload(reDraw);
	var zoom = function(deltaZoom) {
		zoomFactor += deltaZoom;
		if (zoomFactor < 1) zoomFactor = 1;
		if (zoomFactor > 10) zoomFactor = 10;
		centerDiv = document.getElementById('center');
		bpCanvas = document.getElementById('bpCanvas');
		centerDiv.style.width = 100+300*zoomFactor+'px';
		centerDiv.style.height = 100+300*zoomFactor+'px';
		if (zoomFactor <= 3)
			centerDiv.style.margin = '-'+(50+150*zoomFactor)+'px 0 0 -'+(50+150*zoomFactor)+'px';
		bpCanvas.setAttribute('width', 300*zoomFactor);
		bpCanvas.setAttribute('height', 300*zoomFactor);
		reDraw();
	};
	document.addEventListener('keydown', (event) => {
		switch(event.which) {
		case 37: // left
			pos.x -= 1;
			break;
		case 39: // right
			pos.x += 1;
			break;
		case 38: // up
			pos.z -= 1;
			break;
		case 40: // down
			pos.z += 1;
			break;
		case 74: // j
			pos.y -= 1;
			break;
		case 75: // k
			pos.y += 1;
			break;
		case 72: // h
			pos.view = (pos.view == 'x') ? 'y' : ((pos.view == 'y') ? 'z' : 'x');
			break;
		case 76: // l
			pos.view = (pos.view == 'x') ? 'z' : ((pos.view == 'z') ? 'y' : 'x');
			break;
		case 82: // r
			scenes = [];
			reload.reload(reDraw);
			break;
		case 107: // +
			zoom(1);
			break;
		case 109: // -
			zoom(-1);
			break;
		default:
			console.log('>> ' + event.which + ' <<');
			event.preventDefault(); // prevent the default action (scroll / move caret)
			return;
		}
		event.preventDefault(); // prevent the default action (scroll / move caret)
		reDraw();
	});
})();
