/* define default scene */
var sceneDefault = (function() {
	var defaultMaterial = loadTexture( 'under_construction.jpg' );
	var defscene = null;
	var defgroup = null;
	return function(x,y,z) {
		if (defscene == null) {
			defscene = new THREE.Scene();
			var geometry = geoMesh().position([
				0.0, 0.0, 0.0,
				30.0, 0.0, 0.0,
				30.0, 30.0, 0.0
			]).uv([
				0, 0,
				1, 0,
				1, 1
			]).position([
				0.0, 0.0, 0.0,
				30.0, 30.0, 0.0,
				0.0, 30.0, 0.0
			]).uv([
				0, 0,
				1, 1,
				0, 1
			]).build();
			defgroup = new THREE.Object3D();
			var meshFront = new THREE.Mesh( geometry, defaultMaterial );
			defgroup.add(meshFront);
			var meshLeft = new THREE.Mesh( geometry, defaultMaterial );
			meshLeft.rotation.y = Math.PI/2;
			meshLeft.position.z = 30;
			defgroup.add(meshLeft);
			var meshRight = new THREE.Mesh( geometry, defaultMaterial );
			meshRight.rotation.y = -Math.PI/2;
			meshRight.position.x = 30;
			defgroup.add(meshRight);
			var meshBack = new THREE.Mesh( geometry, defaultMaterial );
			meshBack.rotation.y = Math.PI;
			meshBack.position.x = 30;
			meshBack.position.z = 30;
			defgroup.add(meshBack);
			var meshBottom = new THREE.Mesh( geometry, defaultMaterial );
			meshBottom.rotation.x = -Math.PI/2;
			meshBottom.position.z = 30;
			defgroup.add(meshBottom);
			var meshTop = new THREE.Mesh( geometry, defaultMaterial );
			meshTop.rotation.x = Math.PI/2;
			meshTop.position.y = 30;
			defgroup.add(meshTop);
			defgroup.position.x = x*30;
			defgroup.position.y = y*30;
			defgroup.position.z = z*30;
			defscene.add(defgroup);
		} else {
			defgroup.position.x = x*30;
			defgroup.position.y = y*30;
			defgroup.position.z = z*30;
		}
		return defscene;
	};
})();
