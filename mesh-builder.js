/* build mesh, one triangle at a time */
var av3 = (arr) => {
	return {
		x : (pos) => arr[3*pos],
		y : (pos) => arr[3*pos + 1],
		z : (pos) => arr[3*pos + 2],
		v : (pos) => [arr[3*pos], arr[3*pos + 1], arr[3*pos + 2]],
		push : function(arrOne) {
			arr.push(arrOne[0]);
			arr.push(arrOne[1]);
			arr.push(arrOne[2]);
			return this;
		},
		pushAll : function(arrAdd) {
			if (arrAdd.length%3 != 0) return this;
			for (var i=0; i<arrAdd.length; i++)
				arr.push(arrAdd[i]);
			return this;
		},
		slice : (start, end) => arr.slice(3*start, 3*end),
		pop : () => {
			var arrOne = [0, 0, 0];
			arrOne[2] = arr.pop();
			arrOne[1] = arr.pop();
			arrOne[0] = arr.pop();
			return arrOne;
		},
		len : () => arr.length / 3,
		array : () => arr
	};
};

var av2 = (arr) => {
	return {
		x : (pos) => arr[2*pos],
		y : (pos) => arr[2*pos + 1],
		v : (pos) => [arr[2*pos], arr[2*pos + 1]],
		push : function(arrOne) {
			arr.push(arrOne[0]);
			arr.push(arrOne[1]);
			return this;
		},
		pushAll : function(arrAdd) {
			if (arrAdd.length%2 != 0) return this;
			for (var i=0; i<arrAdd.length; i++)
				arr.push(arrAdd[i]);
			return this;
		},
		slice : (start, end) => arr.slice(2*start, 2*end),
		pop : () => {
			var arrOne = [0, 0];
			arrOne[1] = arr.pop();
			arrOne[0] = arr.pop();
			return arrOne;
		},
		len : () => arr.length / 2,
		array : () => arr
	};
};

var v3diff = (a, b) => [b[0]-a[0], b[1]-a[1], b[2]-a[2]];

var v3cross = (ab, ac) => [ ab[1]*ac[2] - ab[2]*ac[1],
							ab[2]*ac[0] - ab[0]*ac[2],
							ab[0]*ac[1] - ab[1]*ac[0] ];

var v3norm = function(v) {
	var mag = Math.sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
	return [v[0]/mag, v[1]/mag, v[2]/mag];
};

var v3eq = (a,b) => a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
var v2eq = (a,b) => a[0] == b[0] && a[1] == b[1];

var v3arc = (vo, va, vb, start, end, len) => {
	var step = (end - start) / len;
	var res = [];
	for (var i=0; i<=len; i++) {
		var angle = start+i*step;
		res.push(vo[0] + va[0]*Math.cos(angle) + vb[0]*Math.sin(angle));
		res.push(vo[1] + va[1]*Math.cos(angle) + vb[1]*Math.sin(angle));
		res.push(vo[2] + va[2]*Math.cos(angle) + vb[2]*Math.sin(angle));
	}
	return res;
};

var v2arc = (vo, va, vb, start, end, len) => {
	var step = (end - start) / len;
	var res = [];
	for (var i=0; i<=len; i++) {
		var angle = start+i*step;
		res.push(vo[0] + va[0]*Math.cos(angle) + vb[0]*Math.sin(angle));
		res.push(vo[1] + va[1]*Math.cos(angle) + vb[1]*Math.sin(angle));
	}
	return res;
};

var geoMesh = function() {
	var indices = av3([]);
	var positions = av3([]);
	var normals = av3([]);
	var uvs = av2([]);

	var pos = false, uv = false, normal = false;

	var processTriangle = function() {
		if (!pos) return;
		// ignore same-point triangles
		if (v3eq(pos.v(0), pos.v(1))
			|| v3eq(pos.v(0), pos.v(2))
			|| v3eq(pos.v(1), pos.v(2)))
			return;
		// calculate normal if not set
		if (!normal) {
			var avNorm = av3([]);
			var comNorm = v3norm(
				v3cross(
					v3diff(pos.v(0), pos.v(1)),
					v3diff(pos.v(0), pos.v(2))));
			avNorm.push(comNorm);
			avNorm.push(comNorm);
			avNorm.push(comNorm);
			normal = avNorm;
		}
		// use default UV if not set
		if (!uv)
			uv = av2([0, 0, 1, 0, 1, 1]);
		// find matched set in previous, add if none matched => add
		var indice = [];
		for (var nvi = 0; nvi<3; nvi++) {
			var foundIndex = false;
			for (var ovi = 0; ovi<positions.len(); ovi++)
				if (v3eq(pos.v(nvi), positions.v(ovi))
					&& v3eq(normal.v(nvi), normals.v(ovi))
					&& v2eq(uv.v(nvi), uvs.v(ovi))) {
					foundIndex = ovi;
					break;
				}
			if (foundIndex === false) {
				foundIndex = positions.len();
				positions.push(pos.v(nvi));
				normals.push(normal.v(nvi));
				uvs.push(uv.v(nvi));
				indice.push(foundIndex);
			} else {
				indice.push(foundIndex);
			}
		}
		indices.push(indice);
	};

	let _this = {
		position : function(posArr) {
			var posVec = av3(posArr);
			if (posVec.len() == 3) {
				processTriangle();
				pos = posVec;
				uv = false;
				normal = false;
			} else
				console.log(">>> Wrong argument " + posArr);
			return _this;
		},
		uv : function(uvArr) {
			var posVec = av2(uvArr);
			if (posVec.len() == 3)
				uv = posVec;
			else
				console.log(">>> Wrong argument " + uvArr);
			return _this;
		},
		normal : function(normalArr) {
			var posVec = av3(normalArr);
			if (posVec.len() == 3)
				normal = posVec;
			else
				console.log(">>> Wrong argument " + normalArr);
			return _this;
		},
		build : function() {
			processTriangle();

			var geometry = new THREE.BufferGeometry();
			geometry.addAttribute( 'position', new THREE.Float32BufferAttribute( new Float32Array(positions.array()), 3 ) );
			geometry.addAttribute( 'normal', new THREE.Float32BufferAttribute( new Float32Array(normals.array()), 3 ) );
			geometry.addAttribute( 'uv', new THREE.Float32BufferAttribute( new Float32Array(uvs.array()), 2 ) );
			geometry.setIndex( new THREE.BufferAttribute( new Uint32Array(indices.array()), 1 ) );
			return geometry;
		},
		span : function(posArr, uvArr, normalArr) {
			var posVec = av3(posArr);
			var uvVec = uvArr ? av2(uvArr) : false;
			var normalVec = normalArr ? av3(normalArr) : false;
			for (var idx=1; idx < posVec.len()-1; idx++) {
				_this.position(av3(posVec.v(0)).push(posVec.v(idx)).push(posVec.v(idx+1)).array());
				if (uvVec)
					_this.uv(av2(uvVec.v(0)).push(uvVec.v(idx)).push(uvVec.v(idx+1)).array());
				if (normalVec)
					_this.normal(av3(normalVec.v(0)).push(normalVec.v(idx)).push(normalVec.v(idx+1)).array());
			}
			return _this;
		},
		stripe : function(posArrA, posArrB, uvArrA, uvArrB, normalArrA, normalArrB) {
			var posVecA = av3(posArrA);
			var uvVecA = uvArrA ? av2(uvArrA) : false;
			var normalVecA = normalArrA ? av3(normalArrA) : false;
			var posVecB = av3(posArrB);
			var uvVecB = uvArrB ? av2(uvArrB) : false;
			var normalVecB = normalArrB ? av3(normalArrB) : false;
			for (var idx=0; idx < posVecA.len()-1; idx++) {
				if (uvVecA && normalVecA)
					_this.span(av3(posVecA.v(idx)).push(posVecB.v(idx)).push(posVecB.v(idx+1)).push(posVecA.v(idx+1)).array(),
								av2(uvVecA.v(idx)).push(uvVecB.v(idx)).push(uvVecB.v(idx+1)).push(uvVecA.v(idx+1)).array(),
								av3(normalVecA.v(idx)).push(normalVecB.v(idx)).push(normalVecB.v(idx+1)).push(normalVecA.v(idx+1)).array());
				else if (uvVecA)
					_this.span(av3(posVecA.v(idx)).push(posVecB.v(idx)).push(posVecB.v(idx+1)).push(posVecA.v(idx+1)).array(),
								av2(uvVecA.v(idx)).push(uvVecB.v(idx)).push(uvVecB.v(idx+1)).push(uvVecA.v(idx+1)).array());
				else if (normalVecA)
					_this.span(av3(posVecA.v(idx)).push(posVecB.v(idx)).push(posVecB.v(idx+1)).push(posVecA.v(idx+1)).array(),
								false,
								av3(normalVecA.v(idx)).push(normalVecB.v(idx)).push(normalVecB.v(idx+1)).push(normalVecA.v(idx+1)).array());
				else
					_this.span(av3(posVecA.v(idx)).push(posVecB.v(idx)).push(posVecB.v(idx+1)).push(posVecA.v(idx+1)).array());
			}
			return _this;
		},
		tube : function(posArrArr, uvArrArr, normalArrArr) {
			for (var idx=0; idx<posArrArr.length-1; idx++) {
				_this.stripe(posArrArr[idx], posArrArr[idx+1],
							 uvArrArr?uvArrArr[idx]:false, uvArrArr?uvArrArr[idx+1]:false,
							 normalArrArr?normalArrArr[idx]:false, normalArrArr?normalArrArr[idx+1]:false);
			}
			return _this;
		},
		box : function(x0, x1, y0, y1, z0, z1) {
			_this.stripe([
				x0, y0, z0,
				x0, y1, z0,
				x1, y1, z0,
				x1, y0, z0,
				x0, y0, z0,
			],[
				x0, y0, z1,
				x0, y1, z1,
				x1, y1, z1,
				x1, y0, z1,
				x0, y0, z1,
			]).stripe([
				x0, y0, z0,
				x1, y0, z0,
			],[
				x0, y1, z0,
				x1, y1, z0,
			]).stripe([
				x0, y0, z1,
				x0, y1, z1,
			],[
				x1, y0, z1,
				x1, y1, z1,
			]);
			return _this;
		},
		cylynder : function(posArrA, posArrB) {
			// stripe ab
			// span a
			// span reversed b
			var av3B = av3([...posArrB]);
			var av3A = av3([...posArrA]);
			var av3RevB = av3([]);
			for (var i=av3B.len()-1; i>=0; i--)
				av3RevB.push(av3B.v(i));
			_this.span(posArrA).span(av3RevB.array());
			av3A.push(av3A.v(0)); // make it closed path
			av3B.push(av3B.v(0)); // make it closed path
			_this.stripe(av3A.array(), av3B.array());
			return _this;
		},
	};
	return _this;
};

var loader = new THREE.TextureLoader();
var loadTexture = function(filePath, repeat) {
	var texture = loader.load(filePath);
	if (repeat) {
		texture.wrapS = THREE.RepeatWrapping;
		texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.set(repeat, repeat);
	}
	return new THREE.MeshBasicMaterial({map: texture});
};

// usage var mesh = new THREE.Mesh( geometry, material );
