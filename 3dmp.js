/*
	This file exports globals that will be modified in scenes
	scenes : the array of scenes, its indexes is the scene position x(y)z divided by 3
	updaters : the list of updaters, each is a function which receive the scene code as param and modify the scene itself
	updateFlag : flag the renderer so that it will update the screen
	camera : the camera object
	actions : the actions flags, indexed from 0 to 9
 */
// three.js code goes here
var scenes; // array x/3:y/3:z/3
var updateFlag = false;
var camera = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 1000 );
camera.rotation.order = 'YXZ';
camera.position.x = camera.position.y = camera.position.z = 1.5;

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

var updaters = []; // array of updater functions
function doUpdates(sceneCode, actionIndex) {
	for (var i=0; i<updaters.length; i++) {
		(updaters[i])(sceneCode, actionIndex);
	}
}
function doReload() {
	scenes = [];
	updaters = [];
	reload.reload(function() {
		updateFlag = true;
	});
}
doReload();

function scenePos() {
	return {x:Math.floor(camera.position.x/3), y:Math.floor(camera.position.y/3), z:Math.floor(camera.position.z/3)};
}

function sceneCode(pos) {
	return pos.x+':'+pos.y+':'+pos.z;
}

function animate() {
	window.setTimeout( function() {
		requestAnimationFrame( animate );
	}, 1000 / 25 );

	// determine the scene base on camera position y | z/ x_
	if (!updateFlag)
		return;
	var threeScene = new THREE.Scene();
	var pos = scenePos();
	var ambientLight = new THREE.AmbientLight( 0xffffff, 0.4 );
	threeScene.add(ambientLight);
		var pointLight = new THREE.PointLight( 0xffffff, 1.1, 9.0 );
		pointLight.position.x = 2.5+pos.x*3.0;
		pointLight.position.z = 2.5+pos.z*3.0;
		pointLight.position.y = 3.0+pos.y*3.0;
		threeScene.add(pointLight);
	[-1,0,1].map((dx)=>[-1,0,1].map((dy)=>[-1,0,1].map((dz)=>{
		var sCode = sceneCode({x:pos.x+dx, y:pos.y+dy, z:pos.z+dz});
		var sceneObj = (sCode in scenes) ? scenes[sCode]('3D') : scene.default('3D') ;
		sceneObj.position.x = 3.0 * (pos.x+dx);
		sceneObj.position.y = 3.0 * (pos.y+dy);
		sceneObj.position.z = 3.0 * (pos.z+dz);
		threeScene.add(sceneObj);
	})));

	renderer.render( threeScene, camera );
	updateFlag = false;
}
requestAnimationFrame( animate );

var directionMove = [ {dx : 0, dz : -0.05},
					{dx : -0.05, dz : 0},
					{dx : 0, dz : 0.05},
					{dx : 0.05, dz : 0} ];
var direction = 0;
var lookY = 0;
var lookX = 0;
document.addEventListener('keydown', (event) => {
	var oldScene = sceneCode(scenePos());
	updateFlag = true;
	switch(event.which) {
	case 82: // r
		doReload();
		break;
	case 37: // left
		direction = (direction + 1) % 4;
		camera.rotation.y = direction*Math.PI/2.0 + lookY*Math.PI/12.0;
		break;
	case 38: // up
		camera.position.x += directionMove[direction].dx;
		camera.position.z += directionMove[direction].dz;
		break;
	case 39: // right
		direction = (direction + 3) % 4;
		camera.rotation.y = direction*Math.PI/2.0 + lookY*Math.PI/12.0;
		break;
	case 40: // down
		camera.position.x -= directionMove[direction].dx;
		camera.position.z -= directionMove[direction].dz;
		break;
	case 72: // h
		leftDir = (direction + 1) % 4;
		camera.position.x += directionMove[leftDir].dx;
		camera.position.z += directionMove[leftDir].dz;
		break;
	case 75: // k
		camera.position.y += 0.1;
		break;
	case 76: // l
		rightDir = (direction + 3) % 4;
		camera.position.x += directionMove[rightDir].dx;
		camera.position.z += directionMove[rightDir].dz;
		break;
	case 74: // j
		camera.position.y -= 0.1;
		break;
	case 100: // np 4
		if (lookY < 4) {
			camera.rotation.y += Math.PI/12.0;
			lookY++;
		}
		break;
	case 104: // np 8 - not work
		if (lookX < 4) {
			lookX++;
			camera.rotation.x = lookX*Math.PI/12.0;
		}
		break;
	case 102: // np 6
		if (lookY > -4) {
			camera.rotation.y -= Math.PI/12.0;
			lookY--;
		}
		break;
	case 98: // np 2 - not work
		if (lookX > -4) {
			lookX--;
			camera.rotation.x = lookX*Math.PI/12.0;
		}
		break;
	case 48: // 0
	case 49: // 1
	case 50: // 2
	case 51: // 3
	case 52: // 4
	case 53: // 5
	case 54: // 6
	case 55: // 7
	case 56: // 8
	case 57: // 9
		var actionIndex = event.which - 48;
		doUpdates(sceneCode(scenePos()), actionIndex);
		break;
	default:
		console.log('>> ' + event.which + ' <<');
		updateFlag = false;
		return; // exit this handler for other keys
	}
	var newScene = sceneCode(scenePos());
	if (oldScene != newScene)
		doUpdates(newScene, -1);
	event.preventDefault(); // prevent the default action (scroll / move caret)
});
